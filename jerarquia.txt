Type
  Fundamental
    Atom
    Indirect
      Pointer
      Array
      Reference
        SourceReference
      Functor
        Method
          Destructor
          Constructor
        Function
 
  User
    Enum
    Alias
    Tuple

Scope
  File
  Class
  Namespace
  Implementation (cuerpo de funciones).

Template

Unit
