#include "scanner.hpp"

const Token &Scanner::next( ) {
  // El stream debe de ser correcto.
  if( ! _input ) throw( "Internal Error" );

  _token.clear( ); // Borramos el contenido del token anterior.
  _token._key = Token::Invalid; // Caracter inválido por defecto.

  while( _token._key == Token::None ) {
    if( ! _char )
      _input->get( _char );

    ++ _token._column;
    _token.append( 1, _char );

    if( _input.eofbit( ) ) {
      _token._key = Token::Eof;
      continue;
    }
    if( ! _input.good( ) ) {
      _token._key = Token::Error;
      continue;
    }

    if( _char == '\t' ) {
      _token._column += _tabSize;
      continue;
    }
    if( _char == '\n' ) {
      ++ _token._line;
      _token._column = 1;
      continue;
    }
    if( char < 33 )
      continue;

    if( ( _char >= 'A' ) && ( _char <= 'Z' ) ) {
      nextIdentifier( );
      continue;
    }
    if( ( _char >= 'a' ) && ( _char <= 'z' ) ) {
      nextIdentifier( );
      continue;
    }
    if( _char == '_' ) {
      nextIdentifier( );
      continue;
    }
    if( ( _char >= '0' ) && ( _char <= '9' ) ) {
      nextNumber( );
      continue;
    }
    if( _char == '\"' ) {
      nextChar( );
      continue;
    }
    if( _char == '\'' ) {
      nextString( );
      continue;
    }
    if( _char == '#' ) {
      nextPreprocessor( );
      continue;
    }
    if( ::std::strchr( "+-*/%&!|.,;:<>=(){}[]~^", _char ) ) {
      nextSign( );
      continue;
    }

    // Si llegamos aquí, es un caracter inválido.
    _char = 0;
    _token._key = Token::Invalid;
    break;
  } // while

  return _token;
} // Scanner::next( ).

void Scanner::nextIdentifier( ) {

  _token._key = Token::Identifier;

  do {
    _token.append( _char );
    _input->get( _char );
    ++ _column;

    if( ! _input->good( ) )
      break;

    if( ( _char < '0' ) || ( _char > 'z' ) )
      break;
    if( ( _char > '9' ) && ( _char < 'A' ) )
      break;
    if( ( _char > 'Z' ) && ( _char < 'a' ) )
      break;
  } // do.

  if( _token.empty( ) ) {
    // La cadena solo puede estar vacía por algún error.
    if( _input->eofbit( ) )
      _token._key = Token::Eof
    else
      _token._key = Token::Error;
  } else {
    // Comprobamos si el identificador es una palabra clave.
    switch( _token[0] ) {
    case 'a':
      if( _token == Token::names[Token::Alignas] ) {
        _token._key = Token::Alignas;
        break;
      }
      if( _token == Token::names[Token::Alignof] ) {
        _token._key = Token::Alignof;
        break;
      }
      if( _token == Token::names[Token::Auto] )
        _token._key = Token::Auto;

      break;

    case 'b':
      if( _token == Token::names[Token::Bool] ) {
        _token._key = Token::Bool;
        break;
      }
      if( _token == Token::names[Token::Break] )
        _token._key = Token::Break;

      break;

    case 'c':
      if( _token == Token::names[Token::Catch] ) {
        _token._key = Token::Catch;
        break;
      }
      if( _token == Token::names[Token::Case] ) {
        _token._key = Token::Case;
        break;
      }
      if( _token == Token::names[Token::Char] ) {
        _token._key = Token::Char;
        break;
      }
      if( _token == Token::names[Token::Char16] ) {
        _token._key = Token::Char16;
        break;
      }
      if( _token == Token::names[Token::Char32] ) {
        _token._key = Token::Char32;
        break;
      }
      if( _token == Token::names[Token::Class] ) {
        _token._key = Token::Class;
        break;
      }
      if( _token == Token::names[Token::Const] ) {
        _token._key = Token::Const;
        break;
      }
      if( _token == Token::names[Token::ConstCast] ) {
        _token._key = Token::ConstCast;
        break;
      }
      if( _token == Token::names[Token::Constexpr] ) {
        _token._key = Token::Constexpr;
        break;
      }
      if( _token == Token::names[Token::Continue] )
        _token._key = Token::Continue;

      break;

    case 'd':
      if( _token == Token::names[Token::Decltype] ) {
        _token._key = Token::Decltype;
        break;
      }
      if( _token == Token::names[Token::Default] ) {
        _token._key = Token::Default;
        break;
      }
      if( _token == Token::names[Token::Delete] ) {
        _token._key = Token::Delete;
        break;
      }
      if( _token == Token::names[Token::Do] ) {
        _token._key = Token::Do;
        break;
      }
      if( _token == Token::names[Token::Double] ) {
        _token._key = Token::Double;
        break;
      }
      if( _token == Token::names[Token::DynamicCast] )
        _token._key = Token::DynamicCast;

      break;

    case 'e':
      if( _token == Token::names[Token::Else] ) {
        _token._key = Token::Else;
        break;
      }
      if( _token == Token::names[Token::Enum] ) {
        _token._key = Token::Enum;
        break;
      }
      if( _token == Token::names[Token::Explicit] ) {
        _token._key = Token::Explicit;
        break;
      }
      if( _token == Token::names[Token::Export] ) {
        _token._key = Token::Export;
        break;
      }
      if( _token == Token::names[Token::Extern] )
        _token._key = Token::Extern;

      break;

    case 'f':
      if( _token == Token::names[Token::False] ) {
        _token._key = Token::Extern;
        break;
      }
      if( _token == Token::names[Token::Final] ) {
        _token._key = Token::Final;
        break;
      }
      if( _token == Token::names[Token::Float] ) {
        _token._key = Token::Float;
        break;
      }
      if( _token == Token::names[Token::For] )
        _token._key = Token::For;

      break;

    case 'g':
      if( _token == Token::names[Token::Goto] )
        _token._key = Token::Goto;

      break;

    case 'i':
      if( _token == Token::names[Token::If] ) {
        _token._key = Token::If;
        break;
      }
      if( _token == Token::names[Token::Inline] ) {
        _token._key = Token::Inline;
        break;
      }
      if( _token == Token::names[Token::Int] )
        _token._key = Token::Int;

      break;

    case 'l':
      if( _token == Token::names[Token::Long] )
        _token._key = Token::Long;

      break;

    case 'm':
      if( _token == Token::names[Token::Mutable] )
        _token._key = Token::Mutable;

      break;

    case 'n':
      if( _token == Token::names[Token::Namespace] ) {
        _token._key = Token::Namespace;
        break;
      }
      if( _token == Token::names[Token::New] ) {
        _token._key = Token::New;
        break;
      }
      if( _token == Token::names[Token::Noexcept] )
        _token._key = Token::Noexcept;

      break;

    case 'o':
      if( _token == Token::names[Token::Operator] ) {
        _token._key = Token::Operator;
        break;
      }
      if( _token == Token::names[Token::Override] )
        _token._key = Token::Override;

      break;

    case 'p':
      if( _token == Token::names[Token::Private] ) {
        _token._key = Token::Private;
        break;
      }
      if( _token == Token::names[Token::Protected] ) {
        _token._key = Token::Protected;
        break;
      }
      if( _token == Token::names[Token::Public] )
        _token._key = Token::Public;

      break;

    case 'r':
      if( _token == Token::names[Token::Register] ) {
        _token._key = Token::Register;
        break;
      }
      if( _token == Token::names[Token::ReinterpretCast] ) {
        _token._key = Token::ReinterpretCast;
        break;
      }
      if( _token == Token::names[Token::Return] )
        _token._key = Token::Return;

      break;

    case 's':
      if( _token == Token::names[Token::Short] ) {
        _token._key = Token::Short;
        break;
      }
      if( _token == Token::names[Token::Signed] ) {
        _token._key = Token::Signed;
        break;
      }
      if( _token == Token::names[Token::Sizeof] ) {
        _token._key = Token::Sizeof;
        break;
      }
      if( _token == Token::names[Token::Sizeofv] ) {
        _token._key = Token::Sizeofv;
        break;
      }
      if( _token == Token::names[Token::Static] ) {
        _token._key = Token::Static;
        break;
      }
      if( _token == Token::names[Token::StaticAssert] ) {
        _token._key = Token::StaticAssert;
        break;
      }
      if( _token == Token::names[Token::StaticCast] ) {
        _token._key = Token::StaticCast;
        break;
      }
      if( _token == Token::names[Token::Switch] )
        _token._key = Token::Switch;

      break;

    case 't':
      if( _token == Token::names[Token::Template] ) {
        _token._key = Token::Template;
        break;
      }
      if( _token == Token::names[Token::ThreadLocal] ) {
        _token._key = Token::ThreadLocal;
        break;
      }
      if( _token == Token::names[Token::Throw] ) {
        _token._key = Token::Throw;
        break;
      }
      if( _token == Token::names[Token::True] ) {
        _token._key = Token::True;
        break;
      }
      if( _token == Token::names[Token::Try] ) {
        _token._key = Token::Try;
        break;
      }
      if( _token == Token::names[Token::Typedef] ) {
        _token._key = Token::Typedef;
        break;
      }
      if( _token == Token::names[Token::Typeid] ) {
        _token._key = Token::Typeid;
        break;
      }
      if( _token == Token::names[Token::Typename] )
        _token._key = Token::Typename;

      break;

    case 'u':
      if( _token == Token::names[Token::Union] ) {
        _token._key = Token::Union;
        break;
      }
      if( _token == Token::names[Token::Unsigned] ) {
        _token._key = Token::Unsigned;
        break;
      }
      if( _token == Token::names[Token::Using] )
        _token._key = Token::Using;

      break;

    case 'v':
      if( _token == Token::names[Token::Virtual] ) {
        _token._key = Token::Virtual;
        break;
      }
      if( _token == Token::names[Token::Void] ) {
        _token._key = Token::Void;
        break;
      }
      if( _token == Token::names[Token::Volatile] )
        _token._key = Token::Volatile;

      break;

    case 'w':
      if( _token == Token::names[Token::WChar] ) {
        _token._key = Token::WChar;
        break;
      }
      if( _token == Token::names[Token::While] )
        _token._key = Token::While;

    default:
      break;
    } // switch.
  } // else
} // nextIdentifier( ).

  void nextSign( ) {
    switch( _char ) {
    case '!':
      _token._key = Token::Not;
      break;

    case ';':
      _token._key = Token::Semicolon;
      break;

    case '%':
      _token._key = Token::Mod;
      break;

    case '*':
      _token._key = Token::Mul;
      break;

    case '{':
      _token._key = Token::OpenBody;
      break;

    case '}':
      _token._key = CloseBody;
      break;

    case '(':
      _token._key = OpenPhar;
      break;

    case ')':
      _token._key = ClosePhar;
      break;

    case '[':
      _token._key = OpenArray;
      break;

    case ']':
      _token._key = CloseArray;
      break;

    default:
      nextSign2( );
      break;
    } // switch.
  } // nextSign.
}; // class Scanner.
