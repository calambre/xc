#include "cmdlineparser.hpp"

#include <cctype>
#include <cassert>
#include <fstream>
#include <sstream>
#include <iostream>

namespace {
  
typedef enum {
  NEED_NONE = 0,
  NEED_DEFINE,
  NEED_UNDEF,
  NEED_SYSPATH,
  NEED_USERPATH,
  NEED_OUTPUT,
  NEED_SYSINCLUDE,
  NEED_USERINCLUDE,
  NEED_TABSIZE,
  NEED_MAX = NEED_TABSIZE
} NeedType;

}

void CmdLineParser::parseStream( ::std::istream &input, int &need ) {
  ::std::string word;
  char character;

  while( ! input.get( character ).eof( ) ) {
    if( ::std::isblank( character ) ) {
      if( ! word.empty( ) ) {
        parseWord( word, need );
        word.clear( );
      }
    } else {
      word.append( 1, character );
    }
  } // while( ( input >> character ).eof( ) ).
  if( ! word.empty( ) )
    parseWord( word, need );
}
void CmdLineParser::parse( int argc, const char **argv ) {
  int need{ NEED_NONE };
  int idx;
  ::std::string word;

  for( idx = 1; idx < argc; ++ idx ) {
    word.assign( argv[idx] );
    parseWord( word, need );
  }

  if( need ) {
    ::std::cerr << "Option\'s require one argument" << ::std::endl;
    ::std::exit( EXIT_FAILURE );
  }
}
void CmdLineParser::parseWord( ::std::string word, int &need ) {
  assert( word.length( ) );
  assert( need < NEED_MAX ); // Imposible e innecesario.

  if( need ) {
    switch( need ) {
    case NEED_DEFINE: // Es una macro a definir. NAME[=VALUE]
      parseDefine( word );
      break;

    case NEED_UNDEF: // Macro a elimina NAME
      undef( word );
      break;

    case NEED_OUTPUT:
      if( _output.empty( ) == false ) {
        ::std::cerr << "Only one output file" << ::std::endl;
        ::std::exit( EXIT_FAILURE );
      }
      _output.assign( word );
      break;

    case NEED_USERPATH:
      addUserPath( word );
      break;

    case NEED_SYSPATH:
      addSysPath( word );
      break;

    case NEED_TABSIZE:
      setTabSize( ::std::stoi( word ) );
      break;

    case NEED_USERINCLUDE:
      addUserInclude( word );
      break;

    case NEED_SYSINCLUDE:
      addSysInclude( word );
      break;

    default:
      // Es imposible que lleguemos aquí.
      ::std::cerr << "Internal error !!" << ::std::endl;
      ::std::abort( );
      break;
    }; // switch( need ).
    need = NEED_NONE;
  } else {
    if( word[0] == '@' ) { // Se nos pasa el nombre de un archio para obtener paramatros de ahí.
      word.erase( 0, 1 );
      if( word.empty( ) ) {
        ::std::cerr << "The @ option requires an input file" << ::std::endl;
        ::std::exit( EXIT_FAILURE );
      }
      ::std::ifstream file( word );

      parseStream( file, need );
      file.close( );

      return;
    } // if( word[0] == '@' ).
    if( word[0] == '-' ) {
      word.erase( 0, 1 );
      if( word == "include" ) {
        need = NEED_USERINCLUDE;
        return;
      }
      if( word == "INCLUDE" ) {
        need = NEED_SYSINCLUDE;
        return;
      }
      switch( word[0] ) {
      case 'd':
        if( word.length( ) == 1 ) {
          need = NEED_DEFINE;
        } else {
          word.erase( 0, 1 );
          parseDefine( word );
          need = NEED_NONE;
        }
        break;

      case 'u':
        if( word.length( ) == 1 ) {
          need = NEED_UNDEF;
        } else {
          word.erase( 0, 1 );
          undef( word );
          need = NEED_NONE;
        }
        break;

      case 't':
        if( word.length( ) == 1 ) {
          need = NEED_TABSIZE;
        } else {
          word.erase( 0, 1 );
          setTabSize( ::std::stoi( word ) );
          need = NEED_NONE;
        }
        break;

      case 'I':
        if( word.length( ) == 1 ) {
          need = NEED_SYSPATH;
        } else {
          word.erase( 0, 1 );
          addSysPath( word );
          need = NEED_NONE;
        }
        break;

      case 'i':
        if( word.length( ) == 1 ) {
          need = NEED_USERPATH;
        } else {
          word.erase( 0, 1 );
          addUserPath( word );
          need = NEED_NONE;
        }
        break;

      case 'v':
        if( word.length( ) > 1 ) {
          ::std::cerr << "Unknown Option " << word << ::std::endl;
          ::std::exit( EXIT_FAILURE );
        }

        version( );
        ::std::exit( EXIT_SUCCESS );
        break;

      case 'h':
        if( word.length( ) > 1 ) {
          ::std::cerr << "Unknown Option " << word << ::std::endl;
          ::std::exit( EXIT_FAILURE );
        }

        help( ::std::cerr );
        ::std::exit( EXIT_FAILURE );
        break;

      default:
        ::std::cerr << "Invalid option -" << word << ::std::endl;
        exit( EXIT_FAILURE );
        break;
      } // switch( word[0] ).
      return;
    } // if( word[0] == '-' ).
    // Es el nombre del archivo de entrada.
    if( _input.empty( ) == false ) {
        ::std::cerr << "Only one input file" << ::std::endl;
        ::std::exit( EXIT_FAILURE );
    }
      _input.assign( word );
  }
}
void CmdLineParser::parseDefine( const ::std::string &word ) {
  assert( word.length( ) );

  ::std::string::size_type position;

  position = word.find( "=" );

  if( position == word.length( ) - 1 ) {
    // Hay un =, pero sin valor. No está permitido.
    ::std::cerr << "Definition with void value " << word << ::std::endl;
    ::std::exit( EXIT_FAILURE );
  }
  if( position == ::std::string::npos ) {
    // No hay valor.
    define( word );
  } else {
    // Hay un valor.
    ::std::string name( word, 0, position );
    ::std::string value( word, position + 1 );
    define( name, value );
  }
} // CmdLineParser::parseDefine( ::std::string & ).
void CmdLineParser::help( ::std::ostream &out ) {
  out << "Usage: ic [options] input-file" << ::std::endl;
  out << "  -o<file>           write output to file rather than stdout" << ::std::endl;
  out << "  -I<dir>            add dir to the include path for system header files" << ::std::endl;
  out << "  -i<dir>            add dir to the include path for user header files" << ::std::endl;
  out << "  -D<macro>[=<def>]  define macro with optional definition" << ::std::endl;
  out << "  -U<macro>          undefine macro" << ::std::endl;
  out << "  -include<file>     prepend #include \"<file>\"" << ::std::endl;
  out << "  -INCLUDE<file>     prepend #include <<file>>" << ::std::endl;
  out << "  -ftabstop=<width>  set the tabstop width size <width>" << ::std::endl;
  out << "  @<file>            read additional options from file" << ::std::endl;
  out << "  -v                 display version of ic" << ::std::endl;
  out << "  -h                 display this help" << ::std::endl;
}
void CmdLineParser::version( ) {
  ::std::cout << "Version" << ::std::endl;
}
std::string CmdLineParser::argv( const ::std::string &arg, int backend ) const {
  switch( backend ) {
  case CmdLineParser::CppBackend:
    return argvCpp( arg );
    break;

  default:
    ::std::cerr << "INTERNAL ERROR" << ::std::endl;
    ::std::cerr << "Function CmdLineParser::argv( ), file cmdlineparser.cpp, line 236" << ::std::endl;
    std::abort( );
  }

  // Para evitar el aviso de función que no devuelve nada.
  return ::std::string( );
}  
std::string CmdLineParser::argvCpp( const ::std::string &arg ) const {
  ::std::string result;
  ::std::stringstream tmp;

  tmp << tabSize( );
  tmp >> result;

  result.insert( 0, " -ftabstop=" );
  result.insert( 0, arg );

  // Los defines.
  for( auto &idx : defines( ) ) {
    result.append( " -D " );
    result.append( idx.name );
    if( ! idx.value.empty( ) ) {
      result.append( "=" );
      result.append( idx.value );
    }
  }
  
  // Los undefs.
  for( auto &idx : undefs( ) ) {
    result.append( " -U " );
    result.append( idx );
  }

  // Las rutas de sistema.
  for( auto &idx : sysPaths( ) ) {
    result.append( " -I " );
    result.append( idx );
  }

  // Las rutas de usuario.
  for( auto &idx : userPaths( ) ) {
    result.append( " -i " );
    result.append( idx );
  }

  // Los includes de sistema.
  for( auto &idx : sysIncludes( ) ) {
    result.append( " -include " );
    result.append( idx );
  }

  // Los includes de usuario.
  for( auto &idx : userIncludes( ) ) {
    result.append( " -include " );
    result.append( idx );
  }

  return ::std::string( result );
}
