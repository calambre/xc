#ifndef TOKENREADER_HPP
#define TOKENREADER_HPP

#include "token.hpp"

#include <istream>

class Scanner {
private:
  ::std::istream *_input;
  char _char; // Último caracter leído.
  Token _token; // Token actual.
  unsigned _tabSize; // Ancho del tabulador.
  unsigned _line;
  unsigned _column;

  void nextIdentifier( );
  void nextSign( );
  void nextSign2( );
  void nextSign3( );

public:
  Scanner( ) noexcept :
    _input( nullptr ),
    _char( 0 ),
    _token( ),
    _tabSize( 8 ),
    _line( 1 ),
    _column( 0 ) {
  }

  inline void setInput( ::std::istream &stream ) noexcept { _input = &stream; }
  inline const ::std::istream *input( ) const { return _input; }
  inline void setTabSize( unsigned ts ) noexcept { _tabSize = ts; }
  inline unsigned tabSize( ) const noexcept { return _tabSize; }
  inline void setLine( unsigned l, unsigned c = 0 ) noexcept {
    _line = l;
    if( c )
      _column = c - 1;
  }
  inline unsigned line( ) const noexcept { return _line; }
  inline unsigned column( ) const noexcept { return _column; }
  const Token &next( );
}; // class Scanner.

#endif
