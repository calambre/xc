XC (Xplorable Compiler)

Procesador de código C++, similar a 'moc' de QT, capaz de añadir propiedades
a nuestras clases, y permitiendo su supervisión o modificación en tiempo
de ejecución.
