#ifndef FSM_HPP
#define FSM_HPP

#include <stdexcept>
#include <algorithm>
#include <functional>

template< typename INPUT, typename RESULT, typename COMPARE = ::std::less< INPUT > >
class FSM {
  friend class Node;

private:
  enum {
    Invalid = 0,
    Result, // 1 001
    Terminal, // 2 010
    Range , // 3 011
    Array, // 4 100
    Set, // 5 101
    Function, // 6 110
    Mask = 7, // 111
    Shift = 3
  };
  struct Test {
    ::std::size_t yes;
    ::std::size_t no;
    inline Test( ::std::size_t y = 0, ::std::size_t n = 0 ) :
      yes( y ),
      no( n ) {
    }
  };
  struct Value: public Test {
    INPUT value;
    inline Value( const INPUT &val, ::std::size_t y = 0, ::std::size_t n = 0 ) :
      Test( y, n ),
      value( val ) {
    }
    inline Value( INPUT &&val, ::std::size_t y = 0, ::std::size_t n = 0 ) :
      Test( y, n ),
      value( val ) {
    }
  };
  struct Range: public Test {
    INPUT min;
    INPUT max;
    inline Range( const INPUT &_min, const INPUT &_max,
                  ::std::size_t y = 0, ::std::size_t n = 0 ) :
      Test( y, n ),
      min( _min ),
      max( _max ) {
    }
    inline Range( INPUT &&_min, INPUT &&_max,
                  ::std::size_t y = 0, ::std::size_t n = 0 ) :
      Test( y, n ),
      min( _min ),
      max( _max ) {
    }
    inline Range( const INPUT &_min, INPUT &&_max,
                  ::std::size_t y = 0, ::std::size_t n = 0 ) :
      Test( y, n ),
      min( _min ),
      max( _max ) {
    }
    inline Range( INPUT &&_min, const INPUT &_max,
                  ::std::size_t y = 0, ::std::size_t n = 0 ) :
      Test( y, n ),
      min( _min ),
      max( _max ) {
    }
  };
  struct Group: public Test {
    const INPUT *first;
    ::std::size_t count;
    inline Group( const INPUT *f, ::std::size_t c,
                  ::std::size_t y = 0, ::std::size_t n = 0 ) noexcept :
      Test( y, n ),
      first( f ),
      count( c ) {
    }
  };
  struct Callback: public Test {
    ::std::function< bool( const INPUT & ) > callback;
    inline Callback( const ::std::function< bool( const INPUT & ) > &f,
                     ::std::size_t y = 0, ::std::size_t n = 0 ) :
      Test( y, n ),
      callback( f ) {
    }
  };
  ::std::vector< RESULT > _results;
  ::std::vector< Value > _values;
  ::std::vector< Range > _ranges;
  ::std::vector< Group > _groups;
  ::std::vector< Callback > _callbacks;
  ::std::size_t _root;

public:
  class Node {
    friend class FSM;
  private:
    FSM< INPUT, RESULT, COMPARE > *_fsm;
    int _idx;
    inline Node( FSM< INPUT, RESULT, COMPARE > *fsm, ::std::size_t i = 0 ) noexcept :
      _fsm( fsm ),
      _idx( i ) {
    }

  public:
    inline Node( ) noexcept :
      _fsm( nullptr ),
      _idx( 0 ) {
    }
    inline Node &operator=( const Node & ) = default;
    inline Node &operator=( Node && ) = default;

    inline bool isValid( ) const noexcept {
      if( ! _fsm ) return false;
      return _idx;
    }
    inline bool isTerminal( ) const noexcept {
      if( ! isValid( ) ) return false;
      return _idx & FSM::Result;
    }
    inline const RESULT &value( ) const {
      if( ! isTerminal( ) ) throw( ::std::logic_error( "No valid node" ) );
      return ( _fsm->_results ).at( _idx >> FSM::Shift );
    }
    Node &eval( const INPUT &val ) {
      if( isValid( ) ) throw( ::std::logic_error( "No valid node" ) );
      switch( _idx & FSM::Mask ) {
      case FSM::Result:
        return *this;
      case FSM::Value:
      case FSM::Range:
      case FSM::Array:
      case FSM::Set:
      case FSM::Function:
      default:
        
      };
    }
  };

public:
  FSM( const RESULT &def = RESULT( ) ) :
    _results( 1, def ),
    _values( ),
    _ranges( ),
    _groups( ),
    _callbacks( ),
    _root( 0 ) {
  }
  FSM( ::std::initializer_list< INPUT > values ) :
    _results( values ),
    _values( ),
    _ranges( ),
    _groups( ),
    _callbacks( ) {
  }
};

#endif
