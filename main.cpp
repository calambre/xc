#include <cctype>
#include <string>
#include <fstream>
#include <iostream>

#include "tokenreader.hpp"
#include "cmdlineparser.hpp"

using namespace std;

int main( int argc, const char **argv ) {
  try {
    CmdLineParser cmdParser;
    string exec;
 
    cmdParser.parse( argc, argv );

    // exec.assign( cmdParser.argv( "cpp -x c++ -std=c++11 " ) );
    //cout << exec << endl;

    if( cmdParser.inputName( ).empty( ) ) {
      // Si no hay archivo de entrada, lo indicamos.
      cerr << "Need an input file" << endl;
      exit( EXIT_FAILURE );
    }

  } catch( std::exception &ex ) {
    cerr << "FATAL !! INTERNAL ERROR " << ex.what( ) << "!!" << endl;
    abort( );
  } catch( ::std::string &str ) {
    cerr << str << endl;
    exit( EXIT_FAILURE );
  } catch( ... ) {
    cerr << "FATAL !! UNEXPECTED ERROR !!" << endl;
    abort( );
  }

  return EXIT_SUCCESS;
}
