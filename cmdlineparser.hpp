#ifndef CMDLINEPARSER_HPP
#define CMDLINEPARSER_HPP

#include <set>
#include <string>
#include <utility>
#include <iostream>
#include <stdexcept>

struct Definition {
  ::std::string name;
  ::std::string value;

  inline Definition( ) noexcept : name( ), value( ) { }
  inline Definition( const ::std::string &n, const ::std::string &v = ::std::string( ) ) : name( n ), value( v ) { }
  inline Definition( const Definition &o ) : name( o.name ), value( o.value ) { }
  inline Definition( Definition &&o ) : name( ::std::move( o.name ) ), value( ::std::move( o.value ) ) { }
  inline Definition &operator=( const Definition &o ) {
    name.assign( o.name );
    value.assign( o.value );
    return *this;
  }
  inline Definition &operator=( Definition &&o ) {
    name.assign( ::std::move( o.name ) );
    value.assign( ::std::move( o.value ) );
    return *this;
  }
};

namespace std {

template< > struct less< Definition > {
  typedef Definition first_argument_type;
  typedef Definition second_argument_type;
  typedef bool result_type;
  inline bool operator( )( const Definition &x, const Definition &y ) const {
    if( x.name.length( ) < y.name.length( ) )
      return true;
    if( x.name.length( ) > y.name.length( ) )
      return false;
    return x.name < y.name;
  }
};

} // namespace std.

class CmdLineParser {
public:
  typedef ::std::set< ::std::string > StringSet;
  typedef ::std::set< Definition > DefinitionSet;

private:
  enum Backend {
    CppBackend = 0
  };

  DefinitionSet _defines; // Macros.
  StringSet _undefs; // Macros a eliminar.
  StringSet _userPaths; // Rutas para los archivos de cabecera de usuario.
  StringSet _sysPaths; // Rutas para los archivos de cabecera del sistema.
  ::std::string _input; // Nombre del archivo de entrada.
  ::std::string _output; // Nombre del archivo de salida. Si vacio, cout.
  StringSet _userIncludes; // #include "" añadidos al inicio del archivo de salida.
  StringSet _sysIncludes; // #include <> añadidos al inicio del archivo de salida.
  int _tabSize; // Tamaño de los tabs.
  bool _generate;

  void parseStream( ::std::istream &, int & );
  void parseWord( ::std::string, int & );
  void parseDefine( const ::std::string & );
  ::std::string argvCpp( const ::std::string & ) const;

public:
  inline const DefinitionSet &defines( ) const noexcept { return _defines; }
  inline const StringSet &undefs( ) const noexcept { return _undefs; }
  inline const StringSet &userPaths( ) const noexcept { return _userPaths; }
  inline const StringSet &sysPaths( ) const noexcept { return _sysPaths; }
  inline const ::std::string inputName( ) const noexcept { return _input; }
  inline const ::std::string outputName( ) const noexcept { return _output; }
  inline const StringSet &userIncludes( ) const noexcept { return _userIncludes; }
  inline const StringSet &sysIncludes( ) const noexcept { return _sysIncludes; }
  inline int tabSize( ) const noexcept { return _tabSize; }
  inline bool generate( ) const noexcept { return _generate; }

  inline CmdLineParser( ) :
    _defines( ),
    _undefs( ),
    _userPaths( ),
    _sysPaths( ),
    _input( ),
    _output( ),
    _userIncludes( ),
    _sysIncludes( ),
    _tabSize( 8 ),
    _generate( true )
  {
    define( "RUN_IC" );
    define( "IC_VERSION", "0.1" );
  }

  inline void define( const ::std::string &name, const ::std::string &value = ::std::string( ) ) {
    _defines.insert( Definition( name, value ) );
  }
  inline void undef( const ::std::string &name ) {
    _undefs.insert( name );
    _defines.erase( Definition( name, ::std::string( ) ) );
  }
  inline void addUserPath( const ::std::string &name ) {
    _userPaths.insert( name );
  }
  inline void addSysPath( const ::std::string &name ) {
    _sysPaths.insert( name );
  }
  inline void setOutputName( const ::std::string &name = ::std::string( ) ) {
    _output.assign( name );
  }
  inline void addUserInclude( const ::std::string &name ) {
    _userIncludes.insert( name );
  }
  inline void addSysInclude( const ::std::string &name ) {
    _sysIncludes.insert( name );
  }
  inline void setTabSize( int ts ) noexcept {
    if( ts > 99 ) {
      ::std::cerr << "TabStop too big" << ::std::endl;
      ::std::exit( EXIT_FAILURE );
    }
    _tabSize = ts;
  }
  inline void disableGeneration( ) noexcept {
    _generate = false;
  }
  void parse( int argc, const char **argv );
  ::std::string argv( const ::std::string &, int = 0 ) const;

  static void help( ::std::ostream & = ::std::cout );
  static void version( );  
};

#endif
