OBJECTS = main.o \
	  cmdlineparser.o \
	  token.o \
	  tokenreader.o

CXX = g++
CXXFLAGS = -std=c++11 -pedantic -Wall -g -ggdb
#CXXFLAGS = -std=c++11 -pedantic -Wall -fvisibility-inline-hidden -O3 -ffast-math
CPPFLAGS =
LDFLAGS =
#LDFLAGS = -pg
LOADLIBES =
LDLIBS =
RM = rm -f
LD = $(CXX)

%.o: %.c

%.o: %.cpp
	@echo Compilando $< para generar $@
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<

.PHONY: all
all: main

.PHONY: clean
clean:
	@echo Limpiando ...
	@$(RM) *.o
	@$(RM) *.gch
	@$(RM) gmon.out
	@$(RM) main

main.o: main.cpp cmdlineparser.hpp tokenreader.hpp token.hpp cmdlineparser.o
cmdlineparser.o: cmdlineparser.hpp cmdlineparser.cpp
tokenreader.o: tokenreader.hpp token.hpp tokenreader.cpp
token.o: token.hpp token.cpp

main: $(OBJECTS)
	@echo Enlazando archivos objeto para generar $@
	@$(LD) $(LDFLAGS) -o $@ $(OBJECTS) $(LOADLIBES) $(LDLIBS)
