#ifndef TOKEN_HPP
#define TOKEN_HPP

#include <string>

class TokenReader;

class Token: public ::std::string {
  friend class TokenReader;

public:
  enum {
    None,         // Token inválido, aún no inicializado.
    Invalid,      // Caracter inválido.
    Error,        // Error en el stream.
    Eol,          // Fín de línea.
    Eof,          // Fín de archivo.

    Identifier,   // Identificador.

    Preprocessor,          // Línea con información del preprocesador.

    FirstSign,             // Primer signo.
    Ellipsis = FirstSign,  // ...
    Point,                 // .
    Semicolon,             // ;
    Colon,                 // :
    Mul,                   // *
    Amp,                   // &
    OpenBody,              // {
    CloseBody,             // }
    OpenPhar,              // (
    ClosePhar,             // )
    OpenArray,             // [
    CloseArray,            // ]
    Div,                   // /
    CharString,            // '
    String,                // "
    Add,                   // +
    Minux,                 // -
    Mod,                   // %
    Greater,               // <
    Less,                  // >
    Not,                   // !
    Assign,                // =
    Bar,                   // |
    Tilde,                 // ~
    Acent,                 // ^
    Question,              // ?
    Inc,                   // ++
    Dec,                   // --
    Equ,                   // ==
    NotEqu,                // !=
    GreaterEq,             // >=
    LessEq,                // <=
    And,                   // &&
    Or,                    // ||
    Left,                  // <<
    Right,                 // >>
    Resolution,            // ::
    UserLiteral,           // ""
    AddAssign,             // +=
    SubAssign,             // -=
    MulAssign,             // *=
    DivAssign,             // /=
    ModAssign,             // %=
    BarAssign,             // |=
    AmpAssign,             // &=
    AndAssign,             // &&=
    OrAssign,              // ||=
    AcentAssign,           // ^=
    TildeAssign,           // ~=
    LeftAssign,            // <<=
    RightAssign,           // >>=
    Deference,             // ->
    DblDeference,          // ->*
    MemberDeference,       // .*
    LineComment,           // //
    BeginComment,          // /*
    EndComment,            // */
    LastSign = EndComment, // Último signo.

    FirstKeyword, // Primera keyword conocida.

    Alignas = FirstKeyword,
    Aligof,
    Auto,
    Bool,
    Break,
    Catch,
    Case,
    Char,
    Char16,
    Char32,
    Class,
    Const,
    ConstCast,
    Constexpr,
    Continue,
    Decltype,
    Default,
    Delete,
    Do,
    Double,
    DynamicCast,
    Else,
    Enum,
    Explicit,
    Export,
    Extern,
    False,
    Final,
    Float,
    For,
    Goto,
    If,
    Inline,
    Int,
    Long,
    Mutable,
    Namespace,
    New,
    Noexcept,
    Nullptr,
    Operator,
    Override,
    Private,
    Protected,
    Public,
    Register,
    ReinterpretCast,
    Return,
    Short,
    Signed,
    Sizeof,
    Sizeofv,
    Static,
    StaticAssert,
    StaticCast,
    Switch,
    Template,
    ThreadLocal,
    Throw,
    True,
    Try,
    Typedef,
    Typeid,
    Typename,
    Union,
    Unsigned,
    Using,
    Virtual,
    Void,
    Volatile,
    WChar,
    While,
    LastKeyword = While // Última keyword.
  };

  static const char *names[Token::LastKeyword];

private:
  int _key;    // Tipo de token.
  int _line;   // Número de línea del archivo actual.
  int _column; // Número de columna del archivo actual.

public:
  inline Token( ) noexcept :
    ::std::string( ),
    _key( Token::Invalid ) {
  }
  inline Token( int k, int l, int c ) noexcept :
    ::std::string( ),
    _key( k ),
    _line( l ),
    _column( c ) {
  }
  inline Token( const Token &o ) :
    ::std::string( o ),
    _key( o._key ),
    _line( o._line ),
    _column( o._column ) {
  }
  inline Token( Token &&o ) :
    ::std::string( o ),
    _key( o._key ),
    _line( o._line ),
    _column( o._column ) {
  }
  inline Token &operator=( const Token &o ) {
    ::std::string::assign( o );
    _key = o._key;
    _line = o._line;
    _column = o._column;
    return *this;
  }
  inline Token &operator=( Token &&o ) {
    ::std::string::assign( o );
    _key = o._key;
    _line = o._line;
    _column = o._column;
    return *this;
  }

  inline bool valid( ) const noexcept { return _key != Token::Invalid; }
  inline bool invalid( ) const noexcept { return _key == Token::Invalid; }
  inline int key( ) const noexcept { return _key; }
  inline int line( ) const noexcept { return _line; }
  inline int column( ) const noexcept { return _column; }
};

#endif
